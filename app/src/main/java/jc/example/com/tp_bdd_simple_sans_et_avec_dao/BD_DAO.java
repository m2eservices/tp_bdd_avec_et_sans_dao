package jc.example.com.tp_bdd_simple_sans_et_avec_dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Jean-Claude on 07/10/2014.
 * <p/>
 *
 * VALABLE UNIQUEMENT POUR LA VERSION 2 (avec DAO)!!!!!!!!!!!!!!!!
 *
 */

public class BD_DAO {

	/* Code pour la version 2 (avec DAO)*/

    private SQLiteDatabase bd;
    private String TABLE_NAME = "personnels";


    public BD_DAO() {
    }

    public void ouverture(Context ctx) {
        bd = new BD(ctx, "personnels.bd", null, 1).getWritableDatabase();
    }

    public void fermeture() {
        bd.close();
    }

    public long ajouter(Personne personne) {
        ContentValues valeurs = new ContentValues();
        valeurs.put("nom", personne.getNom());
        valeurs.put("prenom", personne.getPrenom());
        valeurs.put("age", personne.getAge());
        return bd.insert(TABLE_NAME, null, valeurs);
    }

    public int miseAJour(Personne personne) {
        ContentValues valeurs = new ContentValues();
        valeurs.put("nom", personne.getNom());
        valeurs.put("prenom", personne.getPrenom());
        valeurs.put("age", personne.getAge());
        return bd.update(TABLE_NAME, valeurs, "id = " + personne.getId(), null);
    }

    public int supprimer(Personne personne) {
        return bd.delete(TABLE_NAME, "id = " + personne.getId(), null);
    }

    // renvoie la Personne correspondant à l'id
    public Personne getPersonne(int id) {
        String[] colonnes = {"nom", "prenom", "age"};
        Cursor curseur = bd.query(TABLE_NAME, colonnes, "id = " + id, null, null, null, "nom, prenom");
        if (curseur.getCount() == 0) {
            return null;
        } else {
            curseur.moveToFirst();
            Personne personne = new Personne();
            personne.setId(id);
            personne.setNom(curseur.getString(0));
            personne.setPrenom(curseur.getString(1));
            personne.setAge(curseur.getInt(2));
            curseur.close();
            return personne;
        }

    }

    // renvoie la liste de toutes les Personnes dans la base, triées par Nom+Prénom
    public ArrayList<Personne> getPersonnes() {
        ArrayList<Personne> liste = new ArrayList<Personne>();
        Cursor curseur = bd.query(TABLE_NAME, null, null, null, null, null, "nom, prenom");
        if (curseur.getCount() == 0) {
            return liste;
        } else {

            curseur.moveToFirst();

            do {
                Personne personne = new Personne();
                personne.setId(curseur.getInt(0));
                personne.setNom(curseur.getString(1));
                personne.setPrenom(curseur.getString(2));
                personne.setAge(curseur.getInt(3));
                liste.add(personne);
            }
            while (curseur.moveToNext());

            curseur.close();

            return liste;
        }

    }

}
