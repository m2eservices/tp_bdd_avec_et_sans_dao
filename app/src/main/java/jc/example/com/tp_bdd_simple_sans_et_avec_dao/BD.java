package jc.example.com.tp_bdd_simple_sans_et_avec_dao;

//import android.content.ContentValues;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Jean-Claude on 07/10/2014.
 */

/*
 * UNIQUEMENT POUR LA VERSION 1 (sans DAO)
 *
 * Dans cet exemple, cette classe joue 2 rôles :
 * 	- le "DataBaseHandler" avec onCreate et onUpgrade 
 * 	- le DAO avec open, close, (getDB), add, delete,... 
 * Par conséquent on peut extraire le code de DAO pour le mettre
 * dans la classe BD_DAO, d'où les commentaires dans cette classe (version 2)
 */

public class BD extends SQLiteOpenHelper {

	private String TABLE_NAME = "personnels";

	// version 1 : sans DAO
//	private SQLiteDatabase bd;

	// version 1 : sans DAO
//	public BD(Context ctx) {
//		super(ctx, "personnels.bd", null, 1);
//		bd = getWritableDatabase();
//	}

	// à garder pour les deux versions
	public BD(Context context, String name,
			SQLiteDatabase.CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	// ces deux fonctions (onCreate et onUpgrade) sont à garder absolument ici
	// (héritage de SQLiteOpenHelper)
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_NAME + "("
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "nom TEXT NOT NULL," + "prenom TEXT NOT NULL,"
				+ "age INTEGER NOT NULL);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int ancienneVersion,
			int nouvelleVersion) {
		db.execSQL("DROP TABLE " + TABLE_NAME+ ";");
		onCreate(db);
	}

	// Version 1 : sans DAO
	// Tout le code ci-dessous deviendra inutile quand on le déplacera dans BD_DAO (pour la
	// version 2)
	//

//	public void fermeture() {
//		bd.close();
//	}
//
//	public long ajouter(Personne personne) {
//
//		ContentValues valeurs = new ContentValues();
//
//		valeurs.put("nom", personne.getNom());
//		valeurs.put("prenom", personne.getPrenom());
//		valeurs.put("age", personne.getAge());
//
//		return bd.insert("personnels", null, valeurs);
//	}
//
//	public int miseAJour(Personne personne) {
//
//		ContentValues valeurs = new ContentValues();
//
//		valeurs.put("nom", personne.getNom());
//		valeurs.put("prenom", personne.getPrenom());
//		valeurs.put("age", personne.getAge());
//
//		return bd.update("personnels", valeurs, "id = " + personne.getId(), null);
//	}
//
//	public int supprimer(int id) {
//		return bd.delete("personnels", "id = " + id, null);
//	}
//
//	public Personne getPersonne(int id) {
//
//		Cursor curseur = bd.query("personnels", null, "id = " + id, null, null,	null, null);
//
//		if (curseur.getCount() == 0)
//			return null;
//
//		// théroriquement, il n'y a qu'un seul enregistrement (au plus) qui
//		// répond à la requête...
//		// donc pas besoin de parcourir le résultat de la requête
//		curseur.moveToFirst();
//
//		return curseurToPersonne(curseur);
//	}
//
//	public ArrayList<Personne> getPersonnes() {
//
//		ArrayList<Personne> liste = new ArrayList<Personne>();
//
//		// on renvoie toute la table "personnels", triée par nom+prénom
//		Cursor curseur = bd.query("personnels", null, null, null, null, null, "nom, prenom");
//
//		if (curseur.getCount() == 0)
//			return liste;
//
//		// on parcourt le résultat de la requête et on le transforme en un
//		// tableau qu'on renvoie à la ListeView
//		curseur.moveToFirst();
//		do {
//			liste.add(curseurToPersonne(curseur));
//		} while (curseur.moveToNext());
//
//		curseur.close();
//
//		return liste;
//	}
//
//	private Personne curseurToPersonne(Cursor curseur) {
//
//		Personne personne = new Personne();
//
//		personne.setId(curseur.getInt(0));
//		personne.setNom(curseur.getString(1));
//		personne.setPrenom(curseur.getString(2));
//		personne.setAge(curseur.getInt(3));
//
//		return personne;
//	}

}
