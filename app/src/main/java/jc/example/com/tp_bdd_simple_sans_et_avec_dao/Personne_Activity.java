package jc.example.com.tp_bdd_simple_sans_et_avec_dao;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Jean-Claude on 07/10/2014.
 * 
 * Activité pour saisir UNE personne, mais aussi pour modifier/supprimer UNE
 * personne (passée en paramètre)
 * 
 */
public class Personne_Activity extends Activity {

	// version 1 : sans DAO
//	 private BD bd;

	// version 2 : avec DAO
	 private BD_DAO bd;

	private EditText nom, prenom, age;
	private Personne personne;
	private Button enregistrer, modifier, supprimer;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.une_personne);

		nom = (EditText) findViewById(R.id.nom);
		prenom = (EditText) findViewById(R.id.prenom);
		age = (EditText) findViewById(R.id.age);
		enregistrer = (Button) findViewById(R.id.enregistrer);
		modifier = (Button) findViewById(R.id.modifier);
		supprimer = (Button) findViewById(R.id.supprimer);

		// création d'une nouvelle personne
		personne = new Personne();

		// version 1 : sans DAO
//		 bd = new BD(this);
		 
		// version 2 : avec DAO
		bd = new BD_DAO();
		bd.ouverture(this);
	}

	@Override
	protected void onStart() {
		super.onStart();

		// s'il y a eu passage de paramètre, alors on peut modifier/supprimer
		Bundle donnees = getIntent().getExtras();
		if (donnees != null) {
			enregistrer.setEnabled(false);
			modifier.setEnabled(true);
			supprimer.setEnabled(true);
			personne = bd.getPersonne(donnees.getInt("id"));
			nom.setText(personne.getNom());
			prenom.setText(personne.getPrenom());
			age.setText("" + personne.getAge());
		}
		// sinon on peut ajouter une personne
		else {
			enregistrer.setEnabled(true);
			modifier.setEnabled(false);
			supprimer.setEnabled(false);
		}
	}

	// les boutons sont connectés par XML...

	public void enregistrer(View vue) {

		personne.setNom(nom.getText().toString());
		personne.setPrenom(prenom.getText().toString());
		personne.setAge(Integer.parseInt(age.getText().toString()));

		bd.ajouter(personne);

		finish();
	}

	public void modifier(View vue) {

		personne.setNom(nom.getText().toString());
		personne.setPrenom(prenom.getText().toString());
		personne.setAge(Integer.parseInt(age.getText().toString()));

		bd.miseAJour(personne);

		finish();
	}

	public void supprimer(View vue) {
		// version 1 : sans DAO
//		bd.supprimer(personne.getId());

		// version 2 : avec DAO
		bd.supprimer(personne);

		finish();
	}

	@Override
	protected void onDestroy() {
		bd.fermeture();
		super.onDestroy();
	}
}
