package jc.example.com.tp_bdd_simple_sans_et_avec_dao;


import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Jean-Claude on 07/10/2014.
 * Gestion d'une liste simple de Personnes
 */
public class Liste_des_Personnes_Activity extends ListActivity {
	
	// version 1 : sans DAO
//	private BD bd;

	// version 2 : avec DAO
	private BD_DAO bd;
	
    private ArrayList<Personne> personnes = new ArrayList<Personne>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.les_personnes);
        
        // version 1 : sans DAO
//        bd = new BD(this);

        // version 2 : avec DAO
        bd = new BD_DAO();
        bd.ouverture(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        
        // on récupère la liste des Personnes dans la BDD, sous forme de ArrayList
        personnes = bd.getPersonnes();
        // on affiche la liste dans une ListView
        setListAdapter(new ArrayAdapter<Personne>(this, android.R.layout.simple_list_item_1, personnes));
    }

    @Override
    protected void onListItemClick(ListView liste, View vue, int position, long id) {
        Personne personne = personnes.get(position);
        
        Intent intention = new Intent(this, Personne_Activity.class);
        intention.putExtra("id", personne.getId());
        Log.d("ID","Position = " + position + ", ID = "+id);
        
        startActivity(intention);
    }

    public void edition(View vue) {
        startActivity(new Intent(this, Personne_Activity.class));
    }

    @Override
    protected void onDestroy() {
    	
        bd.fermeture();
        
        super.onDestroy();
    }
}
